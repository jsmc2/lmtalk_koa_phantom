(function() {


	var Koa = require('koa');
	var kapp = Koa();
	var Router = require('koa-router');
	var mount = require('koa-mount');
	var exec = require('co-exec');
	var fs = require('co-fs');
	
	var ChildProcess = require('child_process')


	var phantom_handler = function *(next) {
		var indexOfEscapeFrag = this.request.querystring.indexOf("_escaped_") 
		console.log (">>> indexOfEscapeFrag: ", indexOfEscapeFrag);
		if  ( indexOfEscapeFrag > -1 ) {
			console.log("--")
			console.log("--")
			console.log("--")
			console.log("--")
			console.log("--")
			console.log(">>> #######################################################");
			var ts = Date.now();
			/*	Example calls:
			 * 	http://127.0.0.1:8850/home?_escaped_liumei_crawler_fragment_=
			 *  http://127.0.0.1:8850/school/jmk_m4a_new_sta02/842?_escaped_liumei_crawler_fragment_=
			 */
			
			console.log( '>>> START KOA phantom_handler: ', Date.now() + " | " + JSON.stringify(this.request.query) + " | " +  JSON.stringify(this.request.href) + " | qs " + this.request.querystring);
		
			var vv = {};
			
		
			vv.pwd = yield exec('pwd')
			vv.pwd = vv.pwd.replace(/\r?\n|\r/, '');
			
			vv.requestPath = this.request.path
			if ( vv.requestPath.indexOf('/') === 0 ) {
				 vv.requestPath = vv.requestPath.slice( 1 );
			}
			vv.requestPath = '/' + vv.requestPath || "/nadapath";
			
			vv.saveToFilename = vv.requestPath.replace(/\//g, "FSLASH")+'.html';
			vv.saveTo = vv.pwd+'/Link_to_Rendered/'+ vv.saveToFilename;
			
			vv.execCmd = 'node --harmony Link_to_phantomjs makePageWithPhantom.js  http://pi-www.nanocosm.com' + vv.requestPath + ' ' + vv.saveTo;
			
			console.log( '>>> KOA phantom_handler - execCmd: ', vv.execCmd );
			vv.voidd = yield exec(vv.execCmd,  {maxBuffer: 1024 * 5000});
			console.log( '>>> KOA phantom_handler - execCmd EXECUTED & DONE! ');
			
			vv.fileContent = yield fs.readFile(vv.saveTo, "utf-8");
			console.log('>>> KOA phantom_handler - saved rendered file content retrieved. ');
			///console.log('>>> KOA phantom_handler - fileContent from file: ', vv.fileContent);
		
			
			//	Output to browser {
				this.type = 'text/html; charset=utf-8'
				this.status = 200;
				///this.body = '>>> KOA phantom_handler:  Done!! ' + Date.now() + " | " + this.request.querystring;
				this.body = vv.fileContent;
			//}
		}
	};
	
	
	var zz_all_handler = function *(next){
		console.log("--")
		console.log("--")
		console.log("--")
		console.log("--")
		console.log("--")
		console.log(">>> #######################################################");
		
		console.log( '>>> START KOA all_handler: ', Date.now() + " | " + JSON.stringify(this.request.href) );
		this.type = 'text/html; charset=utf-8'
		this.status = 200;
		this.body = "All Handler - " + JSON.stringify(this.request.href)
	};
	

	
	var router = new Router();
	///router.get('/phantom', all_handler);
	///router.get('/cachefetch', cachefetch_handler);
	router.get('*', phantom_handler);

	kapp.use(mount('/', router.middleware() ) );


	if (!module.parent) {
		kapp.listen(8850);
	}

	console.log('>>> KoaPhantomServer_EXE_8850.js - is running !!!  on http://localhost:8850/');


	
	
})();
