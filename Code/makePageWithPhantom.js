//	====================================================================
//	MAIN:

console.log("-")
console.log("-")
console.log("-")
console.log("-")
console.log(">>> =======================================================");
console.log(">>> TOP of makePageWithPhantom.js");

var page = require('webpage').create(),
  system = require('system'),
  fs = require('fs'),
  timeJustBeforeDelayedOpen, address, saveToPathFilename;
  
page.onError = function (msg, trace) {
    console.log(msg);
    trace.forEach(function(item) {
        console.log('  ', item.file, ':', item.line);
    });
};

if (system.args.length === 1) {
  console.log('Usage: makePageWithPhantom.js <some URL>');
  phantom.exit();
}

timeJustBeforeDelayedOpen = Date.now();
address = system.args[1];
saveToPathFilename = system.args[2]  ||  "yada_"+Date.now()+"";


console.log(">>> Delay run of  delayFnc");
setTimeout(delayFnc, 50)


//	====================================================================
//	HELPERS:

function delayFnc() {
	console.log("-")
	console.log("-")
	console.log(">>> -------------------------------------------------------")
	console.log(">>> Running delayFnc - about to open/retrieve page!");
	page.open(address, function(status) {
	  if (status !== 'success') {
		console.log('FAIL to load the address');
	  } 
	  else {
			console.log('SUCCESS case...');
			console.log('Loading ' + system.args[1]);
			window.setTimeout(
				function() {
					console.log("-")
					console.log("-")
					console.log(">>> - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
					var recordDuration = Date.now() - timeJustBeforeDelayedOpen;
					
					console.log('Record duration: ' + recordDuration + ' msec');
					console.log('saveToPathFilename: ', saveToPathFilename);

					
					var pageContent = page.content
					
					pageContent = pageContent.replace('<script type="text/javascript" src="Assets/Bundles/bundle_vendorJs.js" charset="utf-8"></script>', '');
					
					pageContent = pageContent.replace('<script type="text/javascript" src="Assets/Bundles/bundle_app.js" charset="utf-8"></script>', '');
					
					pageContent = pageContent.replace('aaGlobalee.pgMediator = new aaGlobalee.PgMediatorClass();', '');
					
					///console.log(">>> PHANTOM - makePageWithPhantom.js - pageContent: ", pageContent);
					
					fs.write(saveToPathFilename, pageContent, 'w');
					
					phantom.exit();
				}
				, 8000
			)
	  }
	});

}



